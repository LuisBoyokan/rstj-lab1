#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//[23;21,15,1,1]


int aumentar(int* arr, int n);
void aumentar2(int* arr, int n, int index);
void imprimir(int* arr, int n);
int cumpleQuota(int* pesos, int* coalicion, int n, int quota);
int isProper(int* coalicion, int n, int quota);
int isStrong(int* coalicion, int n, int quota);
int isDecisive(int* coalicion, int n, int quota);
int displayBlocking(int* pesos, int n, int quota);
void complemento(int* coalicion, int* coalicion2, int n);
int isWMinimal(int* pesos, int* coalicion, int n, int quota);
int displayWMinimal(int* pesos, int n, int quota);
int isLMaximal(int* pesos, int* coalicion, int n, int quota);
int displayLMaximal(int* pesos, int n, int quota);
int displayW(int* pesos, int n, int quota);
int displayL(int* pesos, int n, int quota);
int isPlayerCritic(int* pesos, int* coalicion, int numeroJugadores, int quota, int indexJugador);
int isPlayerCriticInGame(int* pesos, int numeroJugadores, int quota, int indexJugador);
int ContarVotos(int* coalicion, int numeroJugadores);
int isPlayerDummyInGame(int* pesos, int numeroJugadores, int quota, int indexJugador);
int isPlayerPasserInGame(int* pesos, int numeroJugadores, int quota, int indexJugador);
int isPlayerDictatorInGame(int* pesos, int n, int quota, int indexJugador);
int isPlayerVeoterInGame(int* pesos, int n, int quota, int index);
int moduleS(int* pesos, int n, int quota, int jugador);
unsigned long long int factorial(int x);
float BanzhafIndex (int* pesos, int numeroJugadores, int quota, int jugador);
float ShapleyShubikIndex (int* pesos, int numeroJugadores, int quota, int indexJugador);

int main(){

	//juego 1 [23;8,7,6,6,6,1,1,1,1,1]
	/*
	int quota = 23;
	int n= 10;
	int pesos[] = {8, 7, 6, 6, 6, 1, 1 ,1 ,1 ,1};
	printf("Quota:%d ", quota);
	imprimir(pesos, n);
	*/
	
	//juego 2 [23;21,15,1,1]
	/*int quota = 23;
	int n = 4;
	int pesos[] = {21,15,1,1};
	*/

	//juego 3 [23;21,15,2]
	/*
	int quota = 23;
	int n = 3;
	int pesos[] = {21,15,2};
	*/
	
	//juego 4 [23;8,7,6,6,6,5]
	/*int quota = 23;
	int n = 6;
	int pesos[] = {8,7,6,6,6,5};
	*/
	
	//juego 5 [25; 8,7,6,6,6,1,1,1,1,1]
	/*int quota = 25;
	int n = 10;
	int pesos[] = {8,7,6,6,6,1,1,1,1,1};
	*/

	//juego 6 [25;21,15,1,1]
	/*int quota = 25;
	int n = 4;
	int pesos[] = {21,15,1,1};
	*/

	//juego 7 [25; 21,15,2]
	/*int quota = 25;
	int n = 3;
	int pesos[] = {21,15,2};
	*/

	//juego 8 [25; 8,7,6,6,6,5]
	/*int quota = 25;
	int n = 6;
	int pesos[] = {8,7,6,6,6,5};
	*/

	//juego 9 [22; 8,7,6,6,6,1,1,1,1,1]
	/*int quota = 22;
	int n = 10;
	int pesos[] = {8,7,6,6,6,1,1,1,1,1};
	*/

	//juego 10 [22;21,15,1,1]
	/*int quota = 22;
	int n = 4;
	int pesos[] = {21,15,1,1};
	*/

	//juego 11 [22; 21,15,2]
	/*int quota = 22;
	int n = 3;
	int pesos[] = {21,15,2};
	*/

	//juego 12 [22; 8,7,6,6,6,5]
	int quota = 22;
	int n = 6;
	int pesos[] = {8,7,6,6,6,5};
	
	
	
	displayW(pesos, n, quota);
	displayL(pesos, n, quota);

	if(isProper(pesos, n, quota)){
		printf("Es propio\n");
	}
	else{printf("No es propio\n");}

	if(isStrong(pesos, n, quota)){
		printf("Es fuerte\n");
	}
	else{printf("No es fuerte\n");}

	if(isDecisive(pesos, n, quota)){
		printf("Es decisivo\n");
	}
	else{printf("No es decisivo\n");}

	
	for(int i=0; i<n; i++){
		printf ("Es dummy(%d)=%d\n",i,isPlayerDummyInGame(pesos, n, quota, i));
	}
	for(int i=0; i<n; i++){
		printf ("Es critico(%d)=%d\n",i,isPlayerCriticInGame(pesos, n, quota, i));
	}
	for(int i=0; i<n; i++){
		printf ("Es disctador(%d)=%d\n",i,isPlayerDictatorInGame(pesos, n, quota, i));
	}
	for(int i=0; i<n; i++){
		printf ("Es passet(%d)=%d\n",i,isPlayerPasserInGame(pesos, n, quota, i));
	}
	for(int i=0; i<n; i++){
		printf ("Es veoter(%d)=%d\n",i,isPlayerVeoterInGame(pesos, n, quota, i));
	}
	
	displayBlocking(pesos, n, quota);
	displayWMinimal(pesos, n, quota);
	displayLMaximal(pesos, n, quota);
	
	for(int i=0; i<n; i++){
		printf("BanzhafIndex(%d)= %f\n", i, BanzhafIndex(pesos, n, quota, i));
	}

	for(int i=0; i<n; i++){
		printf("ShapleyShubikIndex(%d) = %f porc.\n", i, ShapleyShubikIndex(pesos, n, quota, i));
	}
	
	return 0;	
}

//Para todas las coaliciones ganadoras, su complemento es una perdedora.
int isProper(int* pesos, int n, int quota){
	//coalicion para manejar las combinatorias
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i]=0;
	}

	//loop para probar las combinatorias
	int flag1 = 1; //para controlar el while
	while(flag1){
		//si el complemento de una ganadora es ganadora, retornar false.
		if(cumpleQuota(pesos, coalicion, n, quota)){//si cumple quota es ganadora
			complemento(coalicion, coalicion, n);//el complemento
			if(cumpleQuota(pesos, coalicion, n, quota)){//si el complemento de la ganadora es ganadora
				free(coalicion);
				return 0;
			}
			//el complemento de la ganadora es perdedora.
			complemento(coalicion, coalicion, n);//el complemento del complemento, para continuar la iteración donde quedó antes.
		}
		flag1 = aumentar(coalicion, n);
	}
	free(coalicion);
	return 1;
}

//Para todas las coaliciones perdedoras, su complemento es una ganadora.
int isStrong(int* pesos, int n, int quota){
		//coalicion para manejar las combinatorias
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i]=0;
	}

	//loop para probar las combinatorias
	int flag1 = 1; //para controlar el while
	while(flag1){
		//si el complemento de una perdedora es perdedora, retornar false.
		if(!cumpleQuota(pesos, coalicion, n, quota)){//si cumple quota es ganadora
			complemento(coalicion, coalicion, n);//el complemento
			if(!cumpleQuota(pesos, coalicion, n, quota)){//si el complemento de la perdedora es perdedora
				free(coalicion);
				return 0;
			}
			//el complemento de la perdedora es ganadora.
			complemento(coalicion, coalicion, n);//el complemento del complemento, para continuar la iteración donde quedó antes.
		}
		flag1 = aumentar(coalicion, n);
	}
	free(coalicion);
	return 1;
}

int isDecisive(int* pesos, int n, int quota){
	return (isProper(pesos, n, quota) && (isStrong(pesos, n, quota)));
}

int displayBlocking(int* pesos, int n, int quota){
	
	printf("Coaliciones Bloqueantes:\n");
	//coalicion para manejar las combinatorias
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i]=0;
	}

	int count = 0;
	//loop para probar las combinatorias
	int flag1 = 1; //para controlar el while
	int flag2; //imprimir 
	while(flag1){
		flag2 = 0;
		complemento(coalicion, coalicion, n); //el complemento de la coalición.
		if(!cumpleQuota(pesos, coalicion, n, quota)){//el complemento es perdedora
			flag2 = 1;
			count++;
		}
		complemento(coalicion, coalicion, n); //complemento del complemento, para volver a la coalición original.
		if(flag2){imprimir(coalicion, n);} //si se activó la bandera se imprime la coalición bloqueante
		flag1 = aumentar(coalicion, n);
	}
	free(coalicion);
	printf("Total de coaliciones bloqueantes: %d\n", count);
	return count;
}

int displayWMinimal(int* pesos, int n, int quota){
	
	printf("Coaliciones Ganadoras minimales:\n");
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i]=0;
	}
	int count=0;
	int flag1=1;
	while(flag1){//recorrer todas las coaliciones
		if(isWMinimal(pesos, coalicion, n, quota)){//si es minimal
			count++;
			imprimir(coalicion, n);
		}
		flag1 = aumentar(coalicion, n);
	}
	printf("Total de coaliciones ganadoras minimales:%d\n", count);
	free(coalicion);
	return count;
}

int isWMinimal(int* pesos, int* coalicion, int n, int quota){
	if(!cumpleQuota(pesos, coalicion, n, quota)){//es perdedora
		return 0;
	}
	else{//es ganadora
		for(int i=0; i<n; i++){//recorrer todas las posiciones cambiando 1s por 0s para ver si pierde
			if(coalicion[i] == 1){
				coalicion[i]=0;
				if(cumpleQuota(pesos, coalicion, n, quota)){//si despues de quitarle un jugador sigue ganando
					coalicion[i]=1; //restaurar el valor original
					return 0; //retornar que no es minimal
				}
				coalicion[i]=1; //restaurar el valor original
			}
		}
		//recorrió todos y al quitar un jugador perdió en todas, por lo tanto es Wminimal.
		return 1;
	}
}


int displayLMaximal(int* pesos, int n, int quota){
	
	printf("Coaliciones perdedoras maximales:\n");
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i]=0;
	}
	int count=0;
	int flag1=1;
	while(flag1){//recorrer todas las coaliciones
		if(isLMaximal(pesos, coalicion, n, quota)){//si es maximal
			count++;
			imprimir(coalicion, n);
		}
		flag1 = aumentar(coalicion, n);
	}
	printf("Total de coaliciones perdedoras maximales:%d\n", count);
	free(coalicion);
	return count;
}

int isLMaximal(int* pesos, int* coalicion, int n, int quota){
	if(cumpleQuota(pesos, coalicion, n, quota)){//es ganadora
		return 0;
	}
	else{//es perdedora
		for(int i=0; i<n; i++){//recorrer todas las posiciones cambiando 0s por 1s para ver si gana
			if(coalicion[i] == 0){
				coalicion[i]=1;
				if(!cumpleQuota(pesos, coalicion, n, quota)){//si despues de agregarle un jugador sigue perdiendo
					coalicion[i]=0; //restaurar el valor original
					return 0; //retornar que no es maximal
				}
				coalicion[i]=0; //restaurar el valor original
			}
		}
		//recorrió todos y al agregar un jugador ganó en todas, por lo tanto es Lmaximal
		return 1;
	}
}

int displayW(int* pesos, int n, int quota){
	printf("Coaliciones ganadoras:\n");
	//coalicion para manejar las combinatorias
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i]=0;
	}

	//loop para probar las combinatorias
	int flag1 = 1; //para controlar el while
	int count = 0;
	while(flag1){
		if(cumpleQuota(pesos, coalicion, n, quota)){
			count++;
			imprimir(coalicion, n);
		}
		flag1 = aumentar(coalicion, n);
	}
	free(coalicion);
	printf("Total de coaliciones ganadoras:%d\n", count);
	return count;
}

int displayL(int* pesos, int n, int quota){
	printf("Coaliciones perdedoras:\n");
	//coalicion para manejar las combinatorias
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i]=0;
	}

	//loop para probar las combinatorias
	int flag1 = 1; //para controlar el while
	int count = 0;
	while(flag1){
		if(!cumpleQuota(pesos, coalicion, n, quota)){
			count++;
			imprimir(coalicion, n);
		}
		flag1 = aumentar(coalicion, n);
	}
	free(coalicion);
	printf("Total de coaliciones perdedoras:%d\n", count);
	return count;
}


int aumentar(int* arr, int n){
	int count =0;
	for(int i=0; i<n; i++){
		count += arr[i];
	}
	if(count == n){
		return 0;
	}
	else{
		aumentar2(arr, n, n-1);
		return 1;
	}
}
void aumentar2(int* arr,int n, int index){
	if(index>=0){
		if (arr[index] == 0)
		{
			arr[index]=1;
		}
		else
		{
			arr[index]=0;
			aumentar2(arr, n, index-1);
		}
	}
}

void imprimir(int* arr, int n){
	printf("[");
	for(int i=0; i<n; i++){
		printf("%d", arr[i]);
		if(i<n-1){
			printf(", ");
		}
	}
	printf("]\n");
}

int cumpleQuota(int* pesos, int* coalicion, int n, int quota){
	int count = 0;
	for(int i=0; i<n; i++){
		count += pesos[i] * coalicion[i];
	}
	return (count>=quota);
}

/*
deja el complemento de la coalicion en la coalicion2
*/
void complemento(int* coalicion, int* coalicion2, int n){
	for(int i=0; i<n; i++){
		if(coalicion[i] == 0 ){
			coalicion2[i] = 1;
		}
		else{
			coalicion2[i] = 0;
		}
	}
}

/****************Propiedades de los jugadores****************/

int isPlayerCritic(int* pesos, int* coalicion, int numeroJugadores, int quota, int indexJugador) {
	if (coalicion[indexJugador] == 1) {
		if (cumpleQuota(pesos, coalicion, numeroJugadores, quota)) {
			
			coalicion[indexJugador] = 0;

			if (!cumpleQuota(pesos, coalicion, numeroJugadores, quota)) {
				coalicion[indexJugador] = 1;
				return 1;
			}

			coalicion[indexJugador] = 1;
		}
	}

	return 0;
}

int isPlayerCriticInGame(int* pesos, int numeroJugadores, int quota, int indexJugador){
	
	int* coalicion = malloc(numeroJugadores*sizeof(int));
	for(int i=0; i<numeroJugadores; i++){
		coalicion[i]=0;
	}

	int flag1 = 1;
	while(flag1){

		if(isPlayerCritic(pesos, coalicion, numeroJugadores, quota, indexJugador)){
			return 1;
		}

		flag1 = aumentar(coalicion, numeroJugadores);
	}
	free(coalicion);

	return 0;
}

int isPlayerDummyInGame(int* pesos, int numeroJugadores, int quota, int indexJugador){
	int* coalicion = malloc(numeroJugadores*sizeof(int));
	for(int i=0; i<numeroJugadores; i++){
		coalicion[i]=0;
	}

	int flag1 = 1;
	while(flag1){

		if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
			//Si participó en alguna ganadora no es dummy
			if(coalicion[indexJugador] == 1){
				return 0;
			}
		}

		flag1 = aumentar(coalicion, numeroJugadores);
	}
	free(coalicion);

	return 1;
}

int isPlayerPasserInGame(int* pesos, int numeroJugadores, int quota, int indexJugador){
	int* coalicion = malloc(numeroJugadores*sizeof(int));
	for(int i=0; i<numeroJugadores; i++){
		coalicion[i]=0;
	}

	int flag1 = 1;
	while(flag1){

		if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
			//Si existe la coalición donde solo él votó y ganó, es Passer
			if(coalicion[indexJugador] == 1){
				if(ContarVotos(coalicion, numeroJugadores) == 1){
					return 1;
				}
			}
		}

		flag1 = aumentar(coalicion, numeroJugadores);
	}
	free(coalicion);

	return 0;
}

int isPlayerVeoterInGame(int* pesos, int numeroJugadores, int quota, int indexJugador){
	int* coalicion = malloc(numeroJugadores*sizeof(int));
	for(int i=0; i<numeroJugadores; i++){
		coalicion[i]=0;
	}

	int flag1 = 1;
	while(flag1){

		if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
			//Si no existe la coalición donde él no votó y se ganó, es Veoter
			if(coalicion[indexJugador] == 0){
				if(ContarVotos(coalicion, numeroJugadores) == numeroJugadores - 1){
					return 0;
				}
			}
		}

		flag1 = aumentar(coalicion, numeroJugadores);
	}
	free(coalicion);

	return 1;
}

int isPlayerDictatorInGame(int* pesos, int numeroJugadores, int quota, int indexJugador){

	int isPasser = 0;
	int isVeoter = 1;

	int* coalicion = malloc(numeroJugadores*sizeof(int));
	for(int i=0; i<numeroJugadores; i++){
		coalicion[i]=0;
	}

	int flag1 = 1;
	while(flag1){

		if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
			//Si no existe la coalición donde él no votó y se ganó, es Veoter
			if(coalicion[indexJugador] == 0){
				if(ContarVotos(coalicion, numeroJugadores) == numeroJugadores - 1){
					isVeoter = 0;
				}
			}else{

				//Si existe la coalición donde solo él votó y ganó, es Passer			
				if(ContarVotos(coalicion, numeroJugadores) == 1){
					isPasser = 1;
				}
			}
		}

		flag1 = aumentar(coalicion, numeroJugadores);
	}
	free(coalicion);

	return isPasser && isVeoter;
}

int arePlayerSymmetrical(int* pesos, int* coalicion, int numeroJugadores, int quota, int indexJugador1, int indexJugador2) {

	if(pesos[indexJugador1] == pesos[indexJugador2]){
		return 1;
	}

	if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
		if(coalicion[indexJugador1] == 1 && coalicion[indexJugador2] == 0){
			coalicion[indexJugador1] = 0;
			coalicion[indexJugador2] = 1;

			if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
				coalicion[indexJugador1] = 1;
				coalicion[indexJugador2] = 0;
				return 1;
			}

		}else if(coalicion[indexJugador1] == 0 && coalicion[indexJugador2] == 1){
			coalicion[indexJugador1] = 1;
			coalicion[indexJugador2] = 0;

			if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
				coalicion[indexJugador1] = 0;
				coalicion[indexJugador2] = 1;
				return 1;
			}
		}
	}else{
		if(coalicion[indexJugador1] == 0 && coalicion[indexJugador2] == 0){
			coalicion[indexJugador1] = 1;

			if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
				coalicion[indexJugador1] = 0;				
				coalicion[indexJugador2] = 1;

				if(cumpleQuota(pesos, coalicion, numeroJugadores, quota)){
					coalicion[indexJugador2] = 0;
					return 1;
				}
			}
		}
	}

	return 0;
}

/****************Fin Propiedades de los jugadores****************/

/****************Helpers****************/

int ContarVotos(int* coalicion, int numeroJugadores){

	int votos = 0;

	for (int i = 0; i < numeroJugadores; i++)
	{
		votos += coalicion[i];
	}

	return votos;
}

/****************Fin Helpers****************/

int moduleS(int* pesos, int n, int quota, int jugador){
	int* coalicion = malloc(n*sizeof(int));
	for(int i=0; i<n; i++){
		coalicion[i] = 0;
	}

	int count =0;
	int flag1 = 1; //para controlar el while
	while(flag1){ //recorrer todas las coaliciones
		if(cumpleQuota(pesos, coalicion, n, quota)){//es ganadora
			if(isPlayerCritic(pesos, coalicion, n, quota, jugador)){//es critico
				count++;
			}
		}
		flag1 = aumentar(coalicion, n);
	}
	free(coalicion);
	return count;
}

float BanzhafIndex (int* pesos, int numeroJugadores, int quota, int jugador){
	if(jugador>numeroJugadores || jugador<0){
		return -1;
	}
	int Sjugador = moduleS(pesos, numeroJugadores, quota, jugador);
	int Stotal;
	for (int i=0; i < numeroJugadores; i++){
		Stotal += moduleS(pesos, numeroJugadores, quota, i);
	}
	return (float)Sjugador / (float)Stotal;
}

unsigned long long int factorial(int x){
	if(x<0){printf("ERROR: factorial < 0\n");}
	if(x == 0){
		return 1;
	}
	unsigned long long count = 1;
	for(unsigned long long i=1; i<=x; i++){
		count *= i;
	}
	return count;
}

// (Sumatoria de (Xe Si) [(n-|X|)! * (|x|-1)!]   )/n!
float ShapleyShubikIndex (int* pesos, int numeroJugadores, int quota, int indexJugador){
	int* coalicion = malloc(numeroJugadores*sizeof(int));
	for(int i=0; i<numeroJugadores; i++){
		coalicion[i]=0;
	}
	int n = pow(2,numeroJugadores);

	unsigned long long int numerador = 0;
	int flag1 = 1;
	while(flag1){
		if(isPlayerCritic(pesos, coalicion, numeroJugadores, quota, indexJugador)){//si el jugador es critico en la coalición
			//coalición X e Si
			int x = ContarVotos(coalicion, numeroJugadores);
			//printf("x=%d\n",x);
			//printf("n-x! =%d! = %llu\n",n-x ,factorial(n-x));
			//printf("x-1! =%d! = %llu\n",x-1, factorial(x-1));
			numerador += (factorial(n-x) * factorial(x-1));
		} 
		flag1 = aumentar(coalicion, numeroJugadores);
	}
	free(coalicion);
	printf("ShapleyShubikIndex:\n");
	printf("numerador = %llu\n", numerador);
	printf("denominador = %llu\n", factorial(n));
	printf("Aproximadamente = %f\n", ((float)numerador/(float)factorial(n)));
	return (float)numerador / (float)factorial(n); 
	
}